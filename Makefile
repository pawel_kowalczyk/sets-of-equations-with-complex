FLAGS := -c -Wall -pedantic

a.out: main.o complexNumber.o
	g++ main.o complexNumber.o
	rm *.o
	./a.out c < sets/pc.dat

main.o: src/main.cpp inc/vector.hh inc/matrix.hh inc/setOfEquations.hh inc/complexNumber.hh
	g++ $(FLAGS) src/main.cpp

complexNumber.o: src/complexNumber.cpp inc/complexNumber.hh
	g++ $(FLAGS) src/complexNumber.cpp
	