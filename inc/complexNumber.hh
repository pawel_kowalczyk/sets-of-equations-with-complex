#pragma once

#include <iostream>
#include <cmath>
#include "errorCodes.hh"

class ComplexNumber{
private:
    double re;
    double im;

public:
    ComplexNumber(double Re, double Im): re(Re), im(Im) {};
    ComplexNumber() : ComplexNumber(0,0) {};
    ComplexNumber(double Re) : re(Re), im(0) {};
    ComplexNumber(const ComplexNumber& num): re(num.Re()), im(num.Im()) {};
    
    double modul2() const;
    ComplexNumber sprzezenie() const;

    ComplexNumber operator + (const ComplexNumber& lz) const;
    ComplexNumber& operator += (const ComplexNumber& lz);
    ComplexNumber operator - (const ComplexNumber& lz) const;
    ComplexNumber& operator -= (const ComplexNumber& lz);
    ComplexNumber operator * (const ComplexNumber& lz) const;
    ComplexNumber& operator *= (const ComplexNumber& lz);
    ComplexNumber operator * (const double& d) const;
    ComplexNumber& operator *= (const double& d);
    ComplexNumber operator / (const ComplexNumber& lz) const;
    ComplexNumber& operator /= (const ComplexNumber& lz);
    ComplexNumber operator / (double d) const;
    ComplexNumber& operator /= (double d);

    bool operator == (const ComplexNumber& lz) const;
    bool operator != (const ComplexNumber& lz) const;
    bool operator < (const double d) const;
    bool operator > (const double d) const;

    double Re() const;
    double& Re();
    double Im() const;
    double& Im();

    double length() const;
};

std::ostream& operator << (std::ostream& strm, const ComplexNumber& lz);
std::istream& operator >> (std::istream& strm, ComplexNumber& lz);