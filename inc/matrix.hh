#pragma once

#include "vector.hh"
#include <array>
#include <cmath>
#include <algorithm>
#include "errorCodes.hh"

#define EPS 10e-10

using std::cerr;
using std::endl;

// Matrix of N vectors( Vector )
template <typename T, int SIZE>
class Matrix{
private:
    std::array<Vector<T,SIZE>, SIZE> _matrix;

public:
    Matrix() = default;
    Matrix(const Matrix<T,SIZE>& mat) = default;
    Matrix(std::initializer_list<T> in);
    
    Vector<T,SIZE> operator [] (unsigned int idx) const;
    Vector<T,SIZE>& operator [] (unsigned int idx);
    
    T operator () (unsigned int row, unsigned int col) const;
    T& operator () (unsigned int row, unsigned int col);
    
    Matrix<T,SIZE> inverse() const;
    Matrix<T,SIZE> transpose() const;
    Matrix<T,SIZE> replaceColumnWithVector(unsigned int column, const Vector<T,SIZE>& vector) const;
    Matrix<T,SIZE> swapRows(unsigned int row1, unsigned int row2) const;
    Vector<T,SIZE> diagonal(Vector<T,SIZE>& vector) const;
    T determinant() const;
    
    void inverse_ip();
    void transpose_ip();
    void replaceColumnWithVector_ip(unsigned int column, const Vector<T,SIZE>& vector);
    void swapRows_ip(unsigned int row1, unsigned int row2);
    
    Matrix<T,SIZE> operator * (T d) const;  
    Vector<T,SIZE> operator * (const Vector<T,SIZE>& vector) const;
    Matrix<T,SIZE> operator * (const Matrix<T,SIZE>& matrix) const;  
    Matrix<T,SIZE> operator + (const Matrix<T,SIZE>& matrix) const; 
    Matrix<T,SIZE> operator - (const Matrix<T,SIZE>& matrix) const;  

    Matrix<T,SIZE>& operator *= (T d);
    Matrix<T,SIZE>& operator *= (const Matrix<T,SIZE>& matrix);
    Matrix<T,SIZE>& operator += (const Matrix<T,SIZE>& matrix);
    Matrix<T,SIZE>& operator -= (const Matrix<T,SIZE>& matrix);

    std::array<Vector<T,SIZE>, SIZE> getMatrix() const;
    std::array<Vector<T,SIZE>, SIZE>& getMatrix();

};

template <typename T, int SIZE>
Matrix<T,SIZE>::Matrix(std::initializer_list<T> in){
    for(int i=0; i<SIZE; i++)
        for(int j=0; j<SIZE; j++)
            _matrix[i][j] = *(in.begin() + j+i*SIZE);
}

// Returns a reference to Vector with given index
template <typename T, int SIZE>
Vector<T,SIZE>& Matrix<T,SIZE>::operator [] (unsigned int idx){
    if(idx >= SIZE){
        cerr << "Index ["<<idx<<"] out of range for Vector"<<endl;
        exit(ERR_IDX_OUT_OF_RANGE);
    }

    return _matrix[idx];
}

// Returns a Vector with given index
template <typename T, int SIZE>
Vector<T,SIZE> Matrix<T,SIZE>::operator [] (unsigned int idx) const{
    if(idx >= SIZE){
        cerr << "Index ["<<idx<<"] out of range for Vector"<<endl;
        exit(ERR_IDX_OUT_OF_RANGE);
    }

    return _matrix[idx];
}

// Returns a reference to number from matrix with given coords 
template <typename T, int SIZE>
T& Matrix<T,SIZE>::operator () (unsigned int row, unsigned int col){
    if(row >= SIZE || col >= SIZE){
        cerr << "Index ("<< row <<","<< col <<") out of range for Matrix"<<endl;
        exit(ERR_IDX_OUT_OF_RANGE);
    }

    return _matrix[row][col];
}

// Returns a number from matrix with given coords 
template <typename T, int SIZE>
T Matrix<T,SIZE>::operator () (unsigned int row, unsigned int col) const{
    if(row >= SIZE || col >= SIZE){
        cerr << "Index ("<< row <<","<< col <<") out of range for Matrix"<<endl;
        exit(ERR_IDX_OUT_OF_RANGE);
    }

    return _matrix[row][col];
}

// Returns transposed Matrix
template <typename T, int SIZE>
Matrix<T,SIZE> Matrix<T,SIZE>::transpose() const{
    Matrix<T,SIZE> mt = (*this);

    for( int i=0; i<SIZE; i++ ){
        for( int j=i; j<SIZE; j++ ){
            if( i!= j){
                std::swap(mt(i,j), mt(j,i));
            }
        }
    }

    return mt;
}

// Transposes Matrix
template <typename T, int SIZE>
void Matrix<T,SIZE>::transpose_ip(){
    (*this) = (*this).transpose();
}

// Returns a Matrix with swapped column with given Vector
template <typename T, int SIZE>
Matrix<T,SIZE> Matrix<T,SIZE>::replaceColumnWithVector(unsigned int column, const Vector<T,SIZE>& vector) const{
    if(column >= SIZE){
        cerr << "Index ["<<column<<"] out of range for Matrix"<<endl;
        exit(ERR_IDX_OUT_OF_RANGE);
    }

    Matrix<T,SIZE> temp = (*this).transpose();

    temp[column] = vector;
    temp.transpose_ip();

    return temp;
}

// Swaps column with given vector
template <typename T, int SIZE>
void Matrix<T,SIZE>::replaceColumnWithVector_ip(unsigned int column, const Vector<T,SIZE>& vector){
    if(column >= SIZE){
        cerr << "Index ["<<column<<"] out of range for Matrix"<<endl;
        exit(ERR_IDX_OUT_OF_RANGE);
    }

    (*this).transpose_ip();
    (*this)[column] = vector;
    (*this).transpose_ip();
}

//Returns a Matrix with swapped rows of given indexes
template <typename T, int SIZE>
Matrix<T,SIZE> Matrix<T,SIZE>::swapRows(unsigned int row1, unsigned int row2) const{
    if(row1 >= SIZE){
        cerr << "Index ["<< row1 <<"] out of range for Matrix"<<endl;
        exit(ERR_IDX_OUT_OF_RANGE);
    }
    
    else if (row2 >= SIZE){
        cerr << "Index ["<< row2 <<"] out of range for Matrix"<<endl;
        exit(ERR_IDX_OUT_OF_RANGE);
    }

    Matrix<T,SIZE> swappedMatrix = (*this);
    Vector<T,SIZE> temp;
    
    temp = swappedMatrix[row1];
    swappedMatrix[row1] = swappedMatrix[row2];
    swappedMatrix[row2] = temp;

    return swappedMatrix;
}

// Swaps rows
template <typename T, int SIZE>
void Matrix<T,SIZE>::swapRows_ip(unsigned int row1, unsigned int row2){
    (*this) = (*this).swapRows(row1, row2);
}

// Returns a diagonal( Vector ) of Matrix
template <typename T, int SIZE>
Vector<T,SIZE> Matrix<T,SIZE>::diagonal(Vector<T,SIZE>& vector) const{
    Matrix<T,SIZE> temp = (*this);
    Vector<T,SIZE> diag;

    int swapCounter;
    T k;

    for( int r=0; r<SIZE; r++ ){ 
        for( swapCounter=r+1; swapCounter<SIZE; swapCounter++ ){
                if( temp(r,r) < EPS && temp(r,r) > -EPS ){
                    temp.swapRows(r, swapCounter);
                    std::swap(vector[r], vector[swapCounter]);
                }
                else
                    break;
            }

        for( int i=r+1; i<SIZE; i++ ){
            k = temp(i,r)/temp(r,r);
            temp[i] -= temp[r]*k;
            vector[i] -= vector[r]*k;
        }
    }

    for( int r=SIZE-1; r>=0; r-- ){ 
        if( temp(r,r) < EPS && temp(r,r) > -EPS ){
            cerr << "Cannot make diagonal." << endl;
            exit(ERR_DIAGONAL);
        }

        for( int i=r-1; i>=0; i-- ){
            k = temp(i,r)/temp(r,r);
            temp[i] -= temp[r]*k;
            vector[i] -= vector[r]*k;
        }
    }

    for(int i=0; i<SIZE; i++)
        diag[i] = temp(i,i);

    return diag;
}

template <typename T, int SIZE>
Matrix<T,SIZE> Matrix<T,SIZE>::inverse() const{
    Matrix<T,SIZE> temp = (*this);
    Matrix<T,SIZE> inversed;

    // matrix of ones on diagonal
    for(int i=0; i<SIZE; i++){ inversed(i,i) = 1; }

    int swapCounter;
    T k;

    for( int r=0; r<SIZE; r++ ){ 
        for( swapCounter=r+1; swapCounter<SIZE; swapCounter++ ){
            if( temp(r,r) < EPS && temp(r,r) > -EPS ){
                temp.swapRows_ip(r, swapCounter);
                inversed.swapRows_ip(r, swapCounter);
            }
            else
                break;
        }
        // if( swapCounter == SIZE && r != SIZE-1){
        //     cerr << "Cannot inverse matrix" << endl;
        //     exit(ERR_INVERSE_MATRIX);
        // }

        for( int i=r+1; i<SIZE; i++ ){
            k = temp(i,r)/temp(r,r);
            temp[i] -= temp[r]*k;
            inversed[i] -= inversed[r]*k;
        }
    }

    for( int r=SIZE-1; r>=0; r-- ){ 
        if( temp(r,r) < EPS && temp(r,r) > -EPS ){
            cerr << "Cannot inverse matrix" << endl;
            exit(ERR_INVERSE_MATRIX);
        }

        for( int i=r-1; i>=0; i-- ){
            k = temp(i,r)/temp(r,r);
            temp[i] -= temp[r]*k;
            inversed[i] -= inversed[r]*k;
        }
    }

    for(int i=0; i<SIZE; i++){
        if( temp(i,i) != 1 ){
            k = temp(i,i);
            temp(i,i) = temp(i,i)/k;
            inversed[i] = inversed[i]/k; 
        }
    }
    
    return inversed;
}

template <typename T, int SIZE>
void Matrix<T,SIZE>::inverse_ip(){
    (*this) = (*this).inverse();
} 

// Returns a determinant of Matrix and changes given Vector to solve set
template <typename T, int SIZE>
T Matrix<T,SIZE>::determinant() const{
    
    Vector<T,SIZE> diag = diagonal();
    T det = 1;

    for( T& elem : diag.getVector() )
        det *= elem;

    return det;
}

// Returns a Matrix multiplied by a number( double )
template <typename T, int SIZE>
Matrix<T,SIZE> Matrix<T,SIZE>::operator * (T d) const{
    if(d == 0.0) return Matrix();

    Matrix<T,SIZE> temp = (*this);
    for(int i=0; i<SIZE; i++)
        for(int j=0; j<SIZE; j++)
            temp(i,j) *= d;

    return temp;
}

template <typename T, int SIZE>
Matrix<T,SIZE>& Matrix<T,SIZE>::operator *= (T d){
    return (*this) = (*this) * d;
}

// Returns result( Vector ) od multiplying Matrix with a Vector
template <typename T, int SIZE>
Vector<T,SIZE> Matrix<T,SIZE>::operator * (const Vector<T,SIZE>& vector) const{
    if(vector.length() == 0.0) return Vector<T,SIZE>();
    Vector<T,SIZE> result;

    for(int i=0; i<SIZE; i++)
        result[i] = _matrix[i]*vector;

    return result;
}

// Returns result( Matrix ) od multiplying Matrix with a Matrix
template <typename T, int SIZE>
Matrix<T,SIZE> Matrix<T,SIZE>::operator * (const Matrix<T,SIZE>& matrix) const{
    Matrix<T,SIZE> temp = matrix.transpose();
    Matrix<T,SIZE> result;

    for(int j=0; j<SIZE; j++){
        for(int i=0; i<SIZE; i++)
            result[i][j] = (*this)[i]*temp[j];
    }

    return result;
}

template <typename T, int SIZE>
Matrix<T,SIZE>& Matrix<T,SIZE>::operator *= (const Matrix<T,SIZE>& matrix) {
    return (*this) = (*this) * matrix;
}

// Returns result( Matrix ) od adding Matrix with a Matrix
template <typename T, int SIZE>
Matrix<T,SIZE> Matrix<T,SIZE>::operator + (const Matrix<T,SIZE>& matrix) const{
    Matrix<T,SIZE> temp = (*this);

    for(int i=0; i<SIZE; i++)
        for(int j=0; j<SIZE; j++)
            temp(i,j) += matrix(i,j);

    return temp;
}

template <typename T, int SIZE>
Matrix<T,SIZE>& Matrix<T,SIZE>::operator += (const Matrix<T,SIZE>& matrix){
    return (*this) = (*this) + matrix;
}

// Returns result( Matrix ) od subtracting Matrix with a Matrix
template <typename T, int SIZE>
Matrix<T,SIZE> Matrix<T,SIZE>::operator - (const Matrix<T,SIZE>& matrix) const{
    return (*this) + (matrix*-1);
}

template <typename T, int SIZE>
Matrix<T,SIZE>& Matrix<T,SIZE>::operator -= (const Matrix<T,SIZE>& matrix){
    return (*this) = (*this) - matrix;
}

template <typename T, int SIZE>
std::array<Vector<T,SIZE>, SIZE> Matrix<T,SIZE>::getMatrix() const{
    return _matrix;
}

template <typename T, int SIZE>
std::array<Vector<T,SIZE>, SIZE>& Matrix<T,SIZE>::getMatrix(){
    return _matrix;
}

template<typename T, int SIZE>
std::ostream& operator << (std::ostream& strm, const Matrix<T,SIZE>& matrix){
    for(int i=0; i<SIZE; i++)
        strm << "|" << matrix[i] << "|" << endl;

    return strm;
}

template<typename T, int SIZE>
std::istream& operator >> (std::istream& strm, Matrix<T,SIZE>& matrix){
    for(int i=0; i<SIZE; i++)
        strm >> matrix[i];
    
    return strm;
}