#pragma once

#include "matrix.hh"
#include "vector.hh"
#include "errorCodes.hh"

using std::cerr;
using std::cout;
using std::endl;

template <typename T, int SIZE>
class SetOfEquations{
private:
    Matrix<T,SIZE> _matrix;
    Vector<T,SIZE> _absoluteTerm;

    void limitAccuracy(Vector<T,SIZE>& solution);

public:
    SetOfEquations(Matrix<T,SIZE> matrix, Vector<T,SIZE> vector) : _matrix(matrix), _absoluteTerm(vector) {};
    SetOfEquations() : SetOfEquations(Matrix<T,SIZE>(), Vector<T,SIZE>()) {};

    Vector<T,SIZE> solve();

    void showPrettySolution(Vector<T,SIZE> solution) const;
    
    Matrix<T,SIZE> matrix() const;
    Matrix<T,SIZE>& matrix();

    Vector<T,SIZE> vector() const;
    Vector<T,SIZE>& vector();
};

// Solves set using Gaussian elimination and returns solution( Vector3d )
template <typename T, int SIZE>
Vector<T,SIZE> SetOfEquations<T,SIZE>::solve() {    
    Matrix<T,SIZE> inversed = _matrix.inverse();
    Vector<T,SIZE> solution = inversed*_absoluteTerm;
    limitAccuracy(solution);

    return solution;
}

// Prints set with solution 
template <typename T, int SIZE>
void SetOfEquations<T,SIZE>::showPrettySolution(Vector<T,SIZE> solution) const{
    Matrix<T,SIZE> temp = _matrix.transpose();
    cout << endl << "Set with solution: " << endl;
    for(int i=0; i<SIZE; i++)
        cout <<  temp[i] << "| " << solution[i] << " |   | " << _absoluteTerm[i] << " |" << endl;
    
}

// Returns matrix( Matrix3x3 ) of a set 
template <typename T, int SIZE>
Matrix<T,SIZE> SetOfEquations<T,SIZE>::matrix() const{
    return _matrix;
}

// Returns reference to matrix( Matrix3x3 ) of a set
template <typename T, int SIZE>
Matrix<T,SIZE>& SetOfEquations<T,SIZE>::matrix(){
    return _matrix;
}

// Returns reference to absolute term( Vector3d ) of a set
template <typename T, int SIZE>
Vector<T,SIZE>& SetOfEquations<T,SIZE>::vector(){
    return _absoluteTerm;
}

// Returns absolute term( Vector3d ) of a set
template <typename T, int SIZE>
Vector<T,SIZE> SetOfEquations<T,SIZE>::vector() const{
    return _absoluteTerm;
}

// Limits numbers to EPS
template<typename T, int SIZE>
void SetOfEquations<T,SIZE>::limitAccuracy(Vector<T,SIZE>& solution){
    for (T& elem : solution.getVector()){
        if( elem < EPS && elem > -EPS ){
            elem = 0;
        }
    }
}

template <typename T, int SIZE>
std::ostream& operator << (std::ostream& strm, const SetOfEquations<T,SIZE>& set){
    int i;
    Matrix<T,SIZE> temp = set.matrix();

    for(i=0; i<SIZE; i++)
        strm << "| " << temp[i] << " " << "||x_" << i << "|   | " << set.vector()[i] << " |" << endl;

    return strm;
}

template <typename T, int SIZE>
std::istream& operator >> (std::istream& strm, SetOfEquations<T,SIZE>& set){
    strm >> set.matrix() >> set.vector();
    set.matrix().transpose_ip();

    if(strm.fail()){
        cerr << "Could not read set of equations" << endl;
        exit(ERR_READING_SET);
    }

    return strm;
}