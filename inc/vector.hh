#pragma once

#include <iostream>
#include <array>
#include <cmath>
#include "complexNumber.hh"
#include "errorCodes.hh"

using std::cerr;
using std::endl;

template <typename T, int SIZE>
class Vector{
private:
    std::array<T, SIZE> _vector;

public:
    Vector() = default;
    Vector(const Vector<T, SIZE>& vector) = default;
    Vector(std::initializer_list<T> in);

    T& operator [](unsigned int idx);
    T operator [](unsigned int idx) const;

    Vector<T, SIZE> operator + (const Vector<T, SIZE>& vector) const;  
    Vector<T, SIZE> operator - (const Vector<T, SIZE>& vector) const;  
    Vector<T, SIZE> operator * (T d) const; 
    Vector<T, SIZE> operator / (T d) const;
    
    Vector<T, SIZE>& operator += (const Vector<T, SIZE>& vector);
    Vector<T, SIZE>& operator -= (const Vector<T, SIZE>& vector);
    Vector<T, SIZE>& operator *= (T d);
    Vector<T, SIZE>& operator /= (T d);
    
    T operator * (const Vector& vector) const;
    Vector<T, SIZE> vectorProduct(const Vector<T, SIZE>& vector) const;
    
    std::array<T,SIZE> getVector() const;
    std::array<T,SIZE>& getVector();

    double length() const;
};

template <typename T, int SIZE>
Vector<T,SIZE>::Vector(std::initializer_list<T> in){
    std::copy(in.begin(), in.end(), _vector.begin());
}

// Returns a reference to field with given index
template <typename T, int SIZE>
T& Vector<T, SIZE>::operator [](unsigned int idx){
    if(idx >= SIZE){
        cerr << "Index ["<<idx<<"] out of range for Vector" << endl;
        exit(ERR_IDX_OUT_OF_RANGE);
    }

    return _vector[idx];
}

// Returns a field with given index
template <typename T, int SIZE>
T Vector<T, SIZE>::operator [](unsigned int idx) const{
    if(idx >= SIZE){
        cerr << "Index ["<<idx<<"] out of range for Vector" << endl;
        exit(ERR_IDX_OUT_OF_RANGE);
    }

    return _vector[idx];
}

// Returns result( Vector ) of multiplying Vector with a number, order matters
template <typename T, int SIZE>
Vector<T, SIZE> Vector<T, SIZE>::operator * (T d) const{
    Vector<T, SIZE> temp = (*this);

    for(int i=0; i<SIZE; i++)
        temp[i] *= d;

    return temp;
}

// Multiplies vector by given number, returns reference to Vector
template <typename T, int SIZE>
Vector<T, SIZE>& Vector<T, SIZE>::operator *= (T d){
    return (*this) = (*this) * d;
}

// Returns result( Vector ) of adding two Vector
template <typename T, int SIZE>
Vector<T, SIZE> Vector<T, SIZE>::operator + (const Vector<T, SIZE>& vector) const{
    Vector<T, SIZE> temp = (*this);
    for(int i=0; i<SIZE; i++)
        temp[i] += vector[i];

    return temp;
}

// Adds given Vector to this Vector, returns reference to Vector
template <typename T, int SIZE>
Vector<T, SIZE>& Vector<T, SIZE>::operator += (const Vector<T, SIZE>& vector) {
    return (*this) = (*this) + vector;
}

// Return result( Vector ) of subtracting two Vector
template <typename T, int SIZE>
Vector<T, SIZE> Vector<T, SIZE>::operator - (const Vector<T, SIZE>& vector) const{
    return (*this) + (vector*(-1));
}

// Subtracts Vector with given Vector, returns reference to Vector
template <typename T, int SIZE>
Vector<T, SIZE>& Vector<T, SIZE>::operator -= (const Vector<T, SIZE>& vector) {
    return (*this) = (*this) - vector;
}

// Returns result( Vector ) of scalar multiplying two Vector
template <typename T, int SIZE>
T Vector<T, SIZE>::operator * (const Vector<T, SIZE>& vector) const{
    T temp;
    for(int i=0; i<SIZE; i++)
        temp += _vector[i]*vector[i];

    return temp;
}

// Returns result of dividing Vector and a number
template <typename T, int SIZE>
Vector<T, SIZE> Vector<T, SIZE>::operator / (T d) const{
    if(d == 0.f){
        cerr << "Can't divide by 0" << endl;
        exit(ERR_DIV_BY_0);
    }

    Vector<T, SIZE> temp = (*this);
    for(T& elem : temp.getVector())
        elem /= d;

    return temp;
}

template <typename T, int SIZE>
Vector<T, SIZE>& Vector<T, SIZE>::operator /= (T d){
    return (*this) = (*this) / d;
}

// Returns vector product( Vector ) of two vectors( Vector )
template <typename T, int SIZE>
Vector<T, SIZE> Vector<T, SIZE>::vectorProduct(const Vector<T, SIZE>& vector) const{
    Vector<T, SIZE> temp;
    for(int i=0; i<SIZE; i++)
        temp[i] = _vector[i]*vector[i];
    
    return temp;
}

template<>
double Vector<ComplexNumber, 6>::length() const{
    double sum;

    for(const ComplexNumber &elem : _vector)
        sum += elem.modul2();

    return sqrt(sum);
}

template<>
double Vector<ComplexNumber, 5>::length() const{
    double sum;

    for(const ComplexNumber &elem : _vector)
        sum += elem.modul2();

    return sqrt(sum);
}

template<>
double Vector<ComplexNumber, 4>::length() const{
    double sum;

    for(const ComplexNumber &elem : _vector)
        sum += elem.modul2();

    return sqrt(sum);
}

template<>
double Vector<ComplexNumber, 3>::length() const{
    double sum;

    for(const ComplexNumber &elem : _vector)
        sum += elem.modul2();

    return sqrt(sum);
}

template<>
double Vector<ComplexNumber, 2>::length() const{
    double sum;

    for(const ComplexNumber &elem : _vector)
        sum += elem.modul2();

    return sqrt(sum);
}

template<>
double Vector<ComplexNumber, 1>::length() const{
    double sum;

    for(const ComplexNumber &elem : _vector)
        sum += elem.modul2();

    return sqrt(sum);
}

// Returns length of a Vector
template <typename T, int SIZE>
double Vector<T, SIZE>::length() const{
    double sum;

    for(T elem : _vector)
        sum += elem*elem;

    return sqrt(sum);
}

template <typename T, int SIZE>
std::array<T,SIZE> Vector<T,SIZE>::getVector() const{
    return _vector;
}

template <typename T, int SIZE>
std::array<T,SIZE>& Vector<T,SIZE>::getVector(){
    return _vector;
}

template <typename T, int SIZE>
std::ostream& operator << (std::ostream& strm, const Vector<T, SIZE>& vector){
    
    strm << "| ";
    for(int i=0; i<SIZE; i++)
        strm << vector[i] << " ";
    
    strm << "|";

    return strm;
}

template <typename T, int SIZE>
std::istream& operator >> (std::istream& strm, Vector<T, SIZE>& vector){
    for(int i=0; i<SIZE; i++)
        strm >> vector[i];
    return strm;
}