#include "../inc/complexNumber.hh"

using std::cerr;
using std::endl;


double ComplexNumber::modul2() const{
    return re*re + im*im;
}

std::ostream& operator << (std::ostream& strm, const ComplexNumber& lz){
    strm << "(" << lz.Re() << std::showpos << lz.Im() << std::noshowpos << "i)";

	return strm;
}

std::istream& operator >> (std::istream& strm, ComplexNumber& lz){
    char znak;

	strm >> znak;
	if (znak != '(')
		strm.setstate(std::ios_base::failbit);

	strm >> lz.Re() >> lz.Im();
    
    strm >> znak; 
	if (znak != 'i')
		strm.setstate(std::ios_base::failbit);

	strm >> znak;
	if (znak != ')')
		strm.setstate(std::ios_base::failbit);

	return strm;
}

ComplexNumber ComplexNumber::sprzezenie() const{
    return ComplexNumber(re, -im);
}

ComplexNumber ComplexNumber::operator + (const ComplexNumber& lz) const{
    return ComplexNumber(re + lz.Re(), im + lz.Im());
}

ComplexNumber& ComplexNumber::operator += (const ComplexNumber& lz) {
    return (*this) = (*this) + lz;
}

ComplexNumber ComplexNumber::operator - (const ComplexNumber& lz) const{
    return ComplexNumber(re - lz.Re(), im - lz.Im());
}

ComplexNumber& ComplexNumber::operator -= (const ComplexNumber& lz){
    return (*this) = (*this) + lz;
}

ComplexNumber ComplexNumber::operator * (const ComplexNumber& lz) const{
    return ComplexNumber(re * lz.Re() - im * lz.Im(), im * lz.Re() + re * lz.Im());
}

ComplexNumber& ComplexNumber::operator *= (const ComplexNumber& lz){
    return (*this) = (*this) * lz;
}

ComplexNumber ComplexNumber::operator * (const double& d) const{
    return ComplexNumber(re*d, im*d);
}

ComplexNumber& ComplexNumber::operator *= (const double& d){
    return (*this) = (*this) * d;
}

ComplexNumber ComplexNumber::operator / (const ComplexNumber& lz) const{
    return (ComplexNumber(re, im) * lz.sprzezenie()) / lz.modul2();
}

ComplexNumber& ComplexNumber::operator /= (const ComplexNumber& lz){
    return (*this) = (*this) / lz;
}

ComplexNumber ComplexNumber::operator / (double d) const{
    if(d!=0.0f)
	    return ComplexNumber(re / d, im / d);

    else{
        cerr << "Blad dzielenia przez 0" << endl;
        exit(ERR_DIV_BY_0);
    }
}

ComplexNumber& ComplexNumber::operator /= (double d){
    return (*this) = (*this) / d;
}

bool ComplexNumber::operator == (const ComplexNumber& lz) const{
    return (re == lz.Re() && im == lz.Im()) ? true : false;
}

bool ComplexNumber::operator != (const ComplexNumber& lz) const{
    return  ( (*this) == lz ) ? false : true;
}

bool ComplexNumber::operator < (const double d) const{
    return ( (*this).length() < d ) ? true : false; 
}

bool ComplexNumber::operator > (const double d) const{
    return ( (*this).length() > d ) ? true : false; 
}

double ComplexNumber::Re() const{
    return re;
}
double ComplexNumber::Im() const{
    return im;
}

double& ComplexNumber::Re(){
    return re;
}
double& ComplexNumber::Im(){
    return im;
}

double ComplexNumber::length() const{
    return sqrt(re*re + im*im);
}