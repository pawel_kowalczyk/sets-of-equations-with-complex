/*
    Autor: Pawel Kowalczyk
    Indeks: 263491
    Projekt: Uklady rownan liniowych
    Data: 10.04.2022r
*/

#include <iostream>
#include "../inc/vector.hh"
#include "../inc/complexNumber.hh"
#include "../inc/matrix.hh"
#include "../inc/setOfEquations.hh"
#include "../inc/errorCodes.hh"
#include <algorithm>


#define SIZE 3

using std::cin;
using std::cout;
using std::endl;
using std::cerr;


int main(int argc, char* argv[]){

    if( argc < 2 ){
        cerr << "Nie podano typu danych w ukladzie." << endl;
        exit(-3);
    }

    SetOfEquations<ComplexNumber, SIZE> set_c;
    Vector<ComplexNumber, SIZE> solution_c;
    Vector<ComplexNumber, SIZE> inaccuracy_c;

    SetOfEquations<double, SIZE> set_q;
    Vector<double, SIZE> solution_q;
    Vector<double, SIZE> inaccuracy_q;

    switch (argv[1][0])
    {
    case 'c':
        cin >> set_c;
        cout << set_c << endl << endl;

        solution_c = set_c.solve();
        cout << "Solution = " << solution_c << endl;
        
        inaccuracy_c = set_c.matrix()*solution_c - set_c.vector();
        cout << "Inaccuracy = " << inaccuracy_c << endl;
        cout << "Inaccuracy length = " << inaccuracy_c.length() << endl;

        break;
    
    case 'q':
        cin >> set_q;
        cout << set_q << endl << endl;

        solution_q = set_q.solve();
        cout << "Solution = " << solution_q << endl;
        
        inaccuracy_q = set_q.matrix()*solution_q - set_q.vector();
        cout << "Inaccuracy = " << inaccuracy_q << endl;
        cout << "Inaccuracy length = " << inaccuracy_q.length() << endl;       

       break;

    default:
        cerr << "Niezanany typ danych." << endl;
        exit(ERR_UNKNOWN_DATATYPE);
        break;
    }

    return 0;
}